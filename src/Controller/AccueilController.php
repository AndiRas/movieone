<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use App\Entity\Task;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Film;
use App\Entity\Genre;

class AccueilController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function accueil()
    {
        $filmRep = $this->getDoctrine()->getRepository(Film::class);
        $lastFilms = $filmRep->findLastFilms();
        $actions = $filmRep->findLastGenre('Action');
        $comedies = $filmRep->findLastGenre('Comedie');
        $drames  = $filmRep->findLastGenre('Drame');
        return $this->render('accueil/accueil.html.twig',['actions' => $actions, 'comedies'=> $comedies,'drames' => $drames,  'lastFilms' => $lastFilms]);
    }
    /**
     * @Route("/form")
     */
    public function form(){
        $task = new Task();
        $task->setTask('Write a blog post');
        $task->setDueDate(new \DateTime('tomorrow'));

        $form = $this->createFormBuilder($task)
            ->add('task', TextType::class)
            ->add('dueDate', DateType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Task'])
            ->getForm();

        return $this->render('task/task.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/fiche/film-{id<\d+>}.html" ,name="fiche")
     */
    public function fiche(Film $f){
        $f->getImages();
        $f->getGenres();
        return $this->render('fiche/fiche.html.twig',[ 'film' => $f]);
    }

    /**
     * @Route("/liste/films-{page<\d+>}.html", name ="films")
     */
    public function lister($page){
        $max = 5;
        $genres = $this->getDoctrine()->getRepository(Genre::class)->findAll();
        $filmRep = $this->getDoctrine()->getRepository(Film::class);
        $films = $filmRep ->findAllFilms($page,$max);
        $nbPage = $filmRep->getNbPage($max);
        return $this->render('liste/liste.html.twig',[ 'page' => $page, 'nbPage' => $nbPage, 'genres' => $genres ,'films' => $films]);
    }
    /**
     * @Route("/liste/categorie", name ="results")
     */
     public function listeGenre(Request $request){
        $genre = $request->query->get('genre');
        $page = $request->query->get('page');
        if(empty($genre)||$genre == 0){
            return $this->redirectToRoute('films', ['page' => 1]);
        }
        $max = 5;
        $genresRep = $this->getDoctrine()->getRepository(Genre::class);
        $genres = $genresRep->findAll();
        $g = $genresRep->find($genre);
        $filmRep = $this->getDoctrine()->getRepository(Film::class);
        $films = $filmRep ->searchFilms($page,null,$genre,null,$max);
        //var_dump($films);
        $nbPage = $filmRep->getSearchNbPage(null,$genre,$max);
        return $this->render('liste/genre.html.twig',[ 'genre'=> $g,'page' => $page, 'nbPage' => $nbPage, 'genres' => $genres ,'films' => $films]);
     }
}