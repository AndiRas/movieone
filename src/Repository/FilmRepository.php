<?php

namespace App\Repository;

use App\Entity\Film;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Film|null find($id, $lockMode = null, $lockVersion = null)
 * @method Film|null findOneBy(array $criteria, array $orderBy = null)
 * @method Film[]    findAll()
 * @method Film[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Film::class);
    }

    // /**
    //  * @return Film[] Returns an array of Film objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Film
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findLastFilms() : array
    {
        $qb = $this->createQueryBuilder('f')
            ->orderBy('f.sortie','DESC')
            ->setMaxResults(7)
            ->getQuery();
        $films = $qb->execute();
        foreach($films as $f){
            $f->getImages();
        }
        return $films;
    }
    public function findLastGenre($genre) : array
    {
        $qb = $this->createQueryBuilder('f')
            ->innerJoin('f.genres','g')
            ->andWhere('g.nom = :genre')
            ->setParameter('genre',$genre)
            ->orderBy('f.sortie','DESC')
            ->setMaxResults(3)
            ->getQuery();
        $films = $qb->execute();
        return $films;
    }

    public function searchFilms($page,$titre,$genre,$nb = 5){
        if(empty($page)){
            $page = 1;
        }
        $offset = ($page-1)*$nb;
        $qb = $this->createQueryBuilder('f');
        if(!empty($genre)){
            $qb->innerJoin('f.genres','g')
            ->andWhere('g.id = :genre')
            ->setParameter('genre',$genre);
        }
        if(!empty($titre)){
            $qb->andWhere('f.titre = :titre')
                ->setParameter('titre',$titre);
        }
        $qb->orderBy('f.sortie','DESC')
        ->setFirstResult($offset)
        ->setMaxResults($nb);
        $films = $qb->getQuery()->execute();
        foreach($films as $f){
            $f->getImages();
        }
        return $films;
    }
    public function findAllFilms($page,$nb = 5) : array
    {
        if(empty($page)){
            $page = 1;
        }
        $offset = ($page-1)*$nb;
        $qb = $this->createQueryBuilder('f')
        ->orderBy('f.sortie','DESC')
        ->setFirstResult($offset)
        ->setMaxResults($nb);
        $films = $qb->getQuery()->execute();
        foreach($films as $f){
            $f->getImages();
        }
        return $films;
    }
    public function getNbPage($nb = 5)
    {
        $qb = $this->createQueryBuilder('f')
        ->select('count(f.id)');
        $total = $qb->getQuery()->getSingleScalarResult();
        return ceil($total/$nb);
    }
    public function getSearchNbPage($titre,$genre,$nb = 5){
        $qb = $this->createQueryBuilder('f')
        ->select('count(f.id)');
      
        if(!empty($genre)){
            $qb->innerJoin('f.genres','g')
            ->andWhere('g.id = :genre')
            ->setParameter('genre',$genre);
        }
        if(!empty($titre)){
            $qb->andWhere('f.titre = :titre')
                ->setParameter('titre',$titre);
        }
        
        $total = $qb->getQuery()->getSingleScalarResult();
        return ceil($total/$nb);
    }
}
